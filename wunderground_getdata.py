#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# (c) Stefan Waizmann
"""tool for download of historical wunderground weather data as CSV or JSON data files
one file per day, automatically named or save it to sqlite3 database"""

# you might want to change this to your personal keys for default behaviour
WUNDER_ID       = ''
WUNDER_APIKEY   = ""

#   ===========   no changes from here     ===============
try:
    import requests, argparse, os, sys, collections, json, csv, arrow
    from pony import orm
    from datetime import datetime, date, timedelta
except (Exception) as e:
    raise SystemExit(str(e))

_db = orm.Database()

class Station(_db.Entity):
    id = orm.PrimaryKey(int, auto=True)
    wunderground_id = orm.Required(str, unique=True)
    wunderground_key = orm.Required(str, unique=False)
    locate_lat = orm.Optional(float)
    locate_lon = orm.Optional(float)
    model = orm.Optional(str)
    weather = orm.Set('Weather')
    timezone = orm.Optional(str)
    active = orm.Required(bool, default=False)
    SoftwareType = orm.Optional(str)


class Weather(_db.Entity):
    # id = orm.PrimaryKey(int, auto=True)
    station = orm.Required(Station, index='idx_weather')
    dateEpoch = orm.Required(int, size=32, unsigned=True)
    dateLocal = orm.Required(date, index='idx_datelocal')
    TemperatureC = orm.Optional(float)
    DewpointC = orm.Optional(float)
    PressurehPa = orm.Optional(float)
    winddirAvg = orm.Optional(int)
    windspeedAvg = orm.Optional(float)
    windgustAvg = orm.Optional(float)
    Humidity = orm.Optional(float)
    precipRate = orm.Optional(float)
    Conditions = orm.Optional(str)
    Clouds = orm.Optional(str)
    precipTotal = orm.Optional(float)
    SolarRadiationWatts = orm.Optional(float)
    dateUTC = orm.Required(date, index='idx_dateutc')
    # this is added manually
    orm.PrimaryKey(station, dateEpoch)


class WeatherDB(object):
    """An object for init and any requests to the database. Capsules all db access"""
    sqlitefilename = 'wunderground.sqlite3'
    db=_db
    id_active = None    # db id of active wunderground station
    id_selected = None
    wunderground_id = None
    wunderground_key = None
    
    def __init__(self, path, dbtype="sqlite", create_db=False, verbose=0, host="", user="", password=""):
        "initialize database with given path"
        if os.path.isdir(path):
            self.sqlitefilename = os.path.join(os.path.realpath(path), self.sqlitefilename)
        else:
            self.sqlitefilename = path      # permit your own filename
        orm.set_sql_debug(verbose > 1)
        assert dbtype == "sqlite"   # at the moment only sqlite supported
        self.db.bind(provider=dbtype, filename=self.sqlitefilename, create_db=create_db)
        self.db.generate_mapping(create_tables=create_db)
        with orm.db_session:
            s = Station.get(active=True)
            if s:                                                     # found, go with it
                self.id_active = self.id_selected = s.id
                self.wunderground_id = s.wunderground_id
                self.wunderground_key = s.wunderground_key

    def info(self, wunderground_id=''):
        "return information about db content"
        with orm.db_session:
            dmax = dmin = None  # these are min and max dates of weather data
            if not wunderground_id:
                days = orm.count(c.dateLocal for c in Weather )
                dmax = orm.max(c.dateLocal for c in Weather )
                dmin = orm.min(c.dateLocal for c in Weather)
                ids = orm.count(c.wunderground_id for c in Station)
            else:
                days = orm.count(c.dateLocal for c in Weather if c.station.wunderground_id == wunderground_id)
                dmax = orm.max(c.dateLocal for c in Weather if c.station.wunderground_id == wunderground_id)
                dmin = orm.min(c.dateLocal for c in Weather if c.station.wunderground_id == wunderground_id)
                ids = orm.count(c.wunderground_id for c in Station if c.wunderground_id == wunderground_id)
            return days, ids, dmin, dmax

    def activate_id(self, wunderground_id, wunderground_key = None):
        "set an id active"
        with orm.db_session:
            # specific WundergroundID has been provided, search for it. if key provided, set new key
            s = Station.get(wunderground_id=wunderground_id)
            assert s != None, F"WunderID not found in db: '{wunderground_id}'"
            if s:                       # found
                if wunderground_key:    # new key provided
                    s.wunderground_key = wunderground_key
                    o = Station.get(active=True)    # current active shouldnt be active anymore
                    if o:
                        o.active = False 
                    s.active = True     # this one will be active in the future
                orm.commit()            # key modified
                self.id_selected = self.id_active = s.id
                self.wunderground_id = s.wunderground_id
                self.wunderground_key = s.wunderground_key
                return True
            return False

    def select_id(self,  wunderground_id=None, wunderground_key=""):
        "check whether we do have an active WundergroundID. Take this for checking. Make sure new WundergroundID is set active"
        with orm.db_session:
            # specific wunderground_id not found until here. Now look for active ID
            if not wunderground_id:
                s = Station.get(active=True)
                if s:                                                     # found, go with it
                    self.id_active = self.id_selected = s.id
                    self.wunderground_id = s.wunderground_id
                    self.wunderground_key = s.wunderground_key
                    return

            # # specific WundergroundID has been provided, search for it. if key provided, set new key
            # if self.activate_id(wunderground_id, wunderground_key):
            #     return

            # not found. Add new one
            s = Station.get(active=True)    # current active shouldnt be active anymore
            if s:
                s.active = False                
            assert len(wunderground_key) > 5, F"invalid wunderground_key: '{wunderground_key}'"
            S = Station(
                wunderground_id = wunderground_id,
                wunderground_key = wunderground_key,
                active=True,
                SoftwareType = __file__,
            )
            orm.commit()
            s = Station.get(wunderground_id = wunderground_id) # now it should be there
            self.id_active = self.id_selected = s.id
            self.wunderground_id = s.wunderground_id
            self.wunderground_key = s.wunderground_key
            return

    def query_weather_date(self, date1):
        "search whether date exists already"
        with orm.db_session:
            return len(orm.select(w for w in Weather if w.station.id == self.id_selected and w.dateLocal == date1))

    def json_digest(self, jdata, station_id=None, wunderground_key="", force=False):
        "read json weather data and create entries in database"
        date_obs = jdata[0]["obsTimeUtc"]
        # date_tz = jdata[0]["tz"]
        stationid = self.id_active
        if station_id:
            stationid = station_id
        try:
            with orm.db_session:
                if force:
                    # we might have this data already. no update. kill old data and kick in new data of this range
                    ts = []
                    for l in jdata:     # search all epoch entries
                        ts.append(arrow.get(l["obsTimeUtc"]).timestamp)
                    tsmin = min(ts)
                    tsmax = max(ts)
                    query = Weather.select(lambda w: w.station.id == stationid and w.dateEpoch >= tsmin and w.dateEpoch <= tsmax)
                    for w in query:
                        w.delete()
                    orm.commit()

                for l in jdata:     # create new data line
                    w = Weather (
                        station = stationid,
                        dateEpoch = arrow.get(l["obsTimeUtc"]).timestamp,
                        dateLocal = arrow.get(l["obsTimeLocal"]).date(),
                        # dateLocal = datetime.strptime(l["obsTimeLocal"],"%Y-%m-%d %H:%M:%S").date(),
                        TemperatureC = measureitem_fail(l["metric"]["tempAvg"], failout=None),
                        DewpointC = measureitem_fail(l["metric"]["dewptAvg"], failout=None),
                        PressurehPa = measureitem_fail(l["metric"]["pressureMax"], failout=None),
                        winddirAvg = measureitem_fail(l["winddirAvg"], failout=None),
                        windspeedAvg = measureitem_fail(l["metric"]["windspeedAvg"], failout=None),
                        windgustAvg = measureitem_fail(l["metric"]["windgustAvg"], failout=None),
                        Humidity = measureitem_fail(l["humidityAvg"], failout=None),
                        precipRate = measureitem_fail(l["metric"]["precipRate"], failout=None),
                        # Conditions = 
                        # Clouds = 
                        precipTotal = measureitem_fail(l["metric"]["precipTotal"], failout=None),
                        SolarRadiationWatts = measureitem_fail(l["solarRadiationHigh"], failout=None),
                        dateUTC = arrow.get(l["obsTimeUtc"]).date()
                    )
        except Exception as e:
            print(F"you need --force to overwrite values for (UTC): {date_obs}")
            return False
        return True

    def dbverify(self, start, end, force = False):
        "verify whether all dates in date region have data"
        t_start = min(start, end)
        t_end = max(start, end)
        with orm.db_session:
            query = orm.select(c.dateUTC for c in Weather
                    if c.dateUTC >= t_start and c.dateUTC <= t_end
                    and c.station.wunderground_id == self.wunderground_id)
            missingdates = []
            for r in arrow.Arrow.range("day", arrow.get(t_start), arrow.get(t_end), tz=arrow.utcnow().tzinfo):
                if r.date() not in query:
                    missingdates.append(r.date())
                    if not force:
                        print(r.format('YYYY-MM-DD'))
            print(len(query), "dates found,", len(missingdates), "missing")
            return missingdates

def measureitem_fail(val, failout=""):
    "in case the measurement item does not exist (eg solar, wind, rain) convert None to failout"
    if val == None or val == "":
        return failout
    return val


class WindDirection():
    "convert wind degrees to directions and vice versa"
    dirs = ["North", "NNE", "NE", "East", "ESE", "SE", "SSE", "South", "SSW", "SW", "WSW", "West", "WNW", "NW", "NNW"]
    
    def deg2char(self, degrees):
        if degrees == None:
            return ""
        degrees += 360 / len(self.dirs) / 2
        segment = int(degrees // (360 / len(self.dirs)))
        return self.dirs[segment % len(self.dirs)]

    def char2deg(self, direction):
        return self.dirs.index(direction) * 360 / len(self.dirs)


class WundergroundWeather():
    jdata = None
    status_code = None
    wunderground_id = None
    wunderground_key = None
    def __init__(self, wunderground_id, wunderground_key):
        self.wunderground_key = wunderground_key
        self.wunderground_id = wunderground_id
        assert None not in (wunderground_key, wunderground_id), "error: provide wunderground_id and wunderground_key"

    def get(self, t_run):
        "create url and make request"
        url = 'https://api.weather.com/v2/pws/history/all?stationId=' + self.wunderground_id + '&format=json&units=m&date=' + t_run.strftime("%Y%m%d") + "&apiKey=" + self.wunderground_key 
        # print(url)
        r = requests.get(url)
        self.status_code = r.status_code
        if r.ok:
            self.jdata = json.loads(r.text)['observations']
            return len(self.jdata) > 4
        return False
        

if __name__ == "__main__":
    success = collections.namedtuple('Counter',["written","skipped","failed"])
    success.written = success.failed = success.skipped = 0
    yesterday = (date.today()- timedelta(days=1)).isoformat()
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-s','--start', default=yesterday, action='store', help='start with date')
    parser.add_argument('-e','--end', default=yesterday, action='store', help='end date for data search')
    parser.add_argument('-p','--path', default=os.getcwd(), action='store', help='target folder path')
    parser.add_argument('-i','--id', action='store', default=WUNDER_ID, help='the wunderID of the station')
    parser.add_argument('-k','--key', action='store', default=WUNDER_APIKEY, help='your wunderground APIkey')
    parser.add_argument('--force', action='store_true', help='overwrite target files')
    parser.add_argument('--verbose', action='count', default=0, help='tell status while working')
    parser.add_argument('--print', action='store_true', help='print json data, no save')
    dbactgroup = parser.add_mutually_exclusive_group()
    dbactgroup.add_argument('--dbauto', action='store_true', help='fill db automatically')
    dbactgroup.add_argument('--dbinit', action='store_true', help='initialize weather db')
    dbactgroup.add_argument('--dbactive', action='store_true', help='make ID active for future db access')
    dbactgroup.add_argument('--dbinfo', action='store_true', help='info about weather db data')
    dbactgroup.add_argument('--dbverify', action='store_true', help='verify whether all days in db have valid data')
    outgroup = parser.add_mutually_exclusive_group()
    outgroup.add_argument('--json', action='store_true', help='save data as json file')
    outgroup.add_argument('--csv', action='store_true', help='save data as csv file (default)')
    outgroup.add_argument('--db', action='store_true', help='save data into sqlite database')
    
    
    # parse arguments. output to CSV is default
    args = parser.parse_args()
    args.id = args.id.upper()
    if not (args.csv or args.json or args.db or args.dbauto):
        args.csv = True
    
    try:    # check params and whether files are ok
        t_start = arrow.get(args.start).date()
        t_end = arrow.get(args.end).date()
        wdb = None

        if args.dbinit:     # init db and exit
            wdb = WeatherDB(path=args.path, verbose = args.verbose, create_db=True)
            raise SystemExit(F"database initialized: {wdb.sqlitefilename}")
        if args.db or args.dbauto:     # open db
            wdb = WeatherDB(path=args.path, verbose = args.verbose)
            wdb.select_id(wunderground_id=args.id, wunderground_key=args.key)
            args.key = wdb.wunderground_key
            args.id = wdb.wunderground_id
        if args.dbauto:
            args.db = True
            t_start = wdb.info()[3]
            assert wdb.wunderground_id != None, "error: db has not found wundergroundID, run once manually"
        if args.dbactive:
            assert args.id != "", "error: wundergroundID is required"
            wdb = WeatherDB(path=args.path, verbose = args.verbose)
            wdb.activate_id(wunderground_id=args.id, wunderground_key=args.key)
            raise SystemExit(F"wunderID activated for future access: {wdb.wunderground_id}")
        if args.dbinfo:
            wdb = WeatherDB(path=args.path, verbose = args.verbose)
            info = wdb.info(args.id)
            raise SystemExit(F"{arrow.get(info[2]).humanize(arrow.get(info[3]), only_distance=True)} with data [{info[2]}...{info[3]}], {info[1]} IDs, active ID: {wdb.wunderground_id}")
        if args.dbverify:
            # raise NotImplementedError("error: verify not yet implemented")
            wdb = WeatherDB(path=args.path, verbose = args.verbose)
            info = wdb.info(args.id)
            dates = wdb.dbverify(t_start, t_end, args.force)
            wu = WundergroundWeather(wunderground_key=wdb.wunderground_key, wunderground_id=wdb.wunderground_id)
            if args.force:
                counter_mod = 0
                for i in dates:
                    if wu.get(i, wdb.wunderground_id, wdb.wunderground_key):
                        wdb.json_digest(wu.jdata, station_id=wdb.id_selected,wunderground_key=wdb.wunderground_key, force=args.force)
                        counter_mod += 1
                print(F"{counter_mod} dates added")
            raise SystemExit

        # assert os.path.isdir(args.path), F"error: target folder not writeable: {args.path}"
    except Exception as e:
        raise SystemExit(str(e))

    try:
        
        t_run = max(t_start, t_end)
        while min(t_start, t_end) <= t_run:
            json_ext = {True:"json", False:"csv"}
            filename = os.path.join(os.path.realpath(args.path), "{}.{}.{}".format(args.id,t_run.isoformat(),json_ext[args.json]))

            if not args.db and os.path.exists(filename) and (args.csv or args.json) and not args.force:
                print(filename, "already exists", file=sys.stderr)
                t_run -= timedelta(days=1)
                success.skipped += 1
                continue
            if args.db and not args.force and wdb.query_weather_date(t_run):
                print(F"skipping {t_run} because of existing data. Use --force to overwrite", file=sys.stderr)
                success.skipped +=1
                t_run -= timedelta(days=1)
                continue
            
            wu = WundergroundWeather( wunderground_id=args.id, wunderground_key=args.key)
            if not wu.get(t_run):
                print(F"{t_run}: error retrieving data, status {wu.status_code}", file=sys.stderr)
                success.failed += 1 
            else:
                f = sys.stdout  # print to screen is default
                if not (args.print or args.db):
                    f = open(filename, 'w')

                if args.db:
                    if not wdb.json_digest(wu.jdata, station_id=wdb.id_selected, wunderground_key=args.key, force=args.force):
                        success.failed +=1

                if args.json:   # write the whole stuff as json file
                    f.write(json.dumps(wu.jdata))

                if args.csv:           # write as CSV file
                    fieldnames = ["Time","TemperatureC","DewpointC","PressurehPa","WindDirection","WindDirectionDegrees","WindSpeedKMH","WindSpeedGustKMH","Humidity","HourlyPrecipMM","Conditions","Clouds","dailyrainMM","SolarRadiationWatts/m^2","SoftwareType","DateUTC"]
                    writer = csv.DictWriter(f, fieldnames=fieldnames)
                    writer.writeheader()
                    dline = {}
                    wd = WindDirection()

                    for l in wu.jdata:
                        dline["Time"] = measureitem_fail(l["obsTimeLocal"])
                        dline["TemperatureC"] = measureitem_fail(l["metric"]["tempAvg"])
                        dline["DewpointC"] = measureitem_fail(l["metric"]["dewptAvg"])
                        dline["PressurehPa"] = measureitem_fail(l["metric"]["pressureMax"])
                        dline["WindDirection"] = wd.deg2char(l["winddirAvg"])
                        dline["WindDirectionDegrees"] = measureitem_fail(l["winddirAvg"])
                        dline["WindSpeedKMH"] = measureitem_fail(l["metric"]["windspeedAvg"], failout=0) * 3.6
                        dline["WindSpeedGustKMH"] = measureitem_fail(l["metric"]["windgustAvg"], failout=0) * 3.6
                        dline["Humidity"] = measureitem_fail(l["humidityAvg"])
                        dline["HourlyPrecipMM"] = measureitem_fail(l["metric"]["precipRate"])
                        dline["Conditions"] = ""
                        dline["Clouds"] = ""
                        dline["dailyrainMM"] = measureitem_fail(l["metric"]["precipTotal"])
                        dline["SoftwareType"] = __file__
                        dline["DateUTC"] = measureitem_fail(l["obsTimeUtc"])
                        dline["SolarRadiationWatts/m^2"] = measureitem_fail(l["solarRadiationHigh"])

                        writer.writerow(dline)

                if args.verbose:
                    print(t_run.isoformat())

                success.written += 1
                if f != sys.stdout:
                    f.close()	# file is finished
            t_run -= timedelta(days=1)
    except KeyboardInterrupt:
        raise SystemExit("download interrupted")
    except (Exception) as e:
        raise
        print(str(e), file=sys.stderr)
        sys.exit(2)
    print(F"Weather data retrieved: {success.written} days, {success.skipped} days skipped, {success.failed} errors")
