<!-- omit in toc -->
# Get Wunderground data downloaded

Having a weather station connected to [Weather Underground](https://www.wunderground.com/) you might want to have the data yourself on local storage. This tool downloads historical data by wunderground station id of **P**ersonal **W**eather **S**tations (PWS) and saves it to CSV or JSON files on your local harddrive. There is an option to create a database as well.

The tool comes as [python3](https://www.python.org/) code, minimum version is python 3.6. It is a commandline tool, should be working on linux, macOS, Win10.

- [Install](#install)
  - [required libraries](#required-libraries)
- [Help](#help)
- [File type operation](#file-type-operation)
  - [Wunderground ID](#wunderground-id)
  - [Defaults](#defaults)
  - [Example](#example)
- [Messages](#messages)
  - [Status messages](#status-messages)
  - [Error messages](#error-messages)
- [Database operation](#database-operation)
  - [Initialize database](#initialize-database)
  - [First run](#first-run)
  - [Set database active](#set-database-active)
  - [Convenient std call](#convenient-std-call)
  - [Missing data](#missing-data)
  - [Database description](#database-description)

# Install

You need a working python3 environment. Download for free at [python.org](https://www.python.org/downloads/).

## required libraries

make sure following libs are available in your python3 environment:  

* requests, pony, arrow

If not already installed, install required libraries via pip (or pip3 or conda):

```bash
pip install -r requirements.txt
```
# Help

Get help with -h parameter

```
(web) joe@pi:~/src/python/wunderground_getdata$ ./wunderground_getdata.py -h
usage: wunderground_getdata.py [-h] [-s START] [-e END] [-p PATH] [-i ID] [-k KEY]
                               [--force] [--verbose] [--print]
                               [--dbauto | --dbinit | --dbinfo [DBINFO]]
                               [--json | --csv | --db]

tool for downloading of historical wunderground weather data as CSV or JSON data
files one file per day, automatically named or save it to sqlite3 database

optional arguments:
  -h, --help            show this help message and exit
  -s START, --start START
                        start with date
  -e END, --end END     end date for data search
  -p PATH, --path PATH  target folder path
  -i ID, --id ID        the wunderID of the station
  -k KEY, --key KEY     your wunderground APIkey
  --force               overwrite target files
  --verbose             tell status while working
  --print               print json data, no save
  --dbauto              fill db automatically
  --dbinit              initialize weather db
  --dbinfo              info about weather db
  --dbverify            find weather data gaps in db
  --json                save data as json file
  --csv                 save data as csv file (default)
  --db                  save data into sqlite database
(web) joe@pi:~/src/python/wunderground_getdata$ 

```

# File type operation

## Wunderground ID

Every PWS (personal weather station) feeding [wunderground](https://www.wunderground.com/) has an ID, looks typically similar to "IBAYERNF3". If you want to operate a PWS, this station will have a unique WundergroundID. If you do not have a WundergroundID, you might obtain an ID from [wunderground.com](https://www.wunderground.com/).

The APIkey is you permission to access the data of the world of weather underground. Get it for free from wunderground. This personal key is your permission to access data and it is necessary for this program.

## Defaults

By default, just by typing ´python wunderground_getdata.py´ on the commandline wunderground_getdata.py (has to be in current folder) will download yesterday's data only.
If you provide one date, it'll create data files for all days between today and your given day.
With 2 dates provided, it will create the data files for all days between your 2 given days.

**It is necessary to provide a station id and APIkey**.

Default output option is CSV files.

## Example

```bash
(web) joe@pi:~ $ ./wunderground_getdata.py -s 2013-02-02 -e 2013-02-04 --id IBAYERNL8 -p weatherdata
```

the line above will download weather data from station id IBAYERNL8. It will cover 3 days (from Feb 2 to Feb 4, 2013).
Target directory for all CSV files will be "weatherdata" folder, located in the current directory.
Without -p the files will be saved in current folder.

# Messages

## Status messages

after data download, the program will come back with a status message like:


Weather data retrieved: 0 days, 0 days skipped, 1 errors


- count of *days* is the number of successful downloaded days with weather data.
- count of *skipped* is the count of days where exisiting data has been found in the database  use the --force parameter to overwrite
- count of *errors* is the number of days without correct answer from wunderground server. Reasons might be invalid key or no weather data found.

## Error messages

*SyntaxError: invalid syntax* is raised if your python version is lower 3.6.
-> install proper python version

```bash
(web) joe@pi:~ $ wunderground_getdata.py --db
Database file is not found: '/home/joe/wunderground.sqlite3'
(web) joe@pi:~ $ 
```

Database is not initialized or path provided is missing/wrong.
-> move to folder with database or the *--path* param has to point to correct folder

# Database operation

If you want to use this script with database support instead creating a single file for every day, use *--db* param. All station and weather data will be saved in a single db file (sqlite).

## Initialize database

Initialize the database with *--dbinit* param. Make sure your path *-p path* provided is pointing to the correct folder. Without *--path* the database file will be created in current folder.

```bash
(web) joe@pi:~ $ wunderground_getdata.py --dbinit -p ./wetter
database initialized: /home/joe/wetter/wunderground.sqlite3
(web) joe@pi:~ $ 
```

## First run

On the first run it is necessary to provide wundergroundID and your Wunderground APIkey. If you get a **401 error** it is most likely your APIkey from wunderground is wrong/has a typo or your connection to the internet is down.

```bash
(web) joe@pi:~ $ wunderground_getdata.py -i IBAYERNL8 --key 609877d6ba5a832e --db -p ./wetter
2019-07-12: error retrieving data, status 401
(web) joe@pi:~ $ 
```

For following requests, you wundergroundID and its key are saved. Now you can request data without providing id and key:

```bash
(web) joe@pi:~ $ wunderground_getdata.py --verbose --db -p wetter -s 2019-07-12
2019-07-13
2019-07-12
Weather data retrieved: 2 days, 0 days skipped, 0 errors
(web) joe@pi:~ $
```

## Set database active

If you run *wunderground_getdata.py* with --db in database mode (might be necessary to provide *--path* if database is not in current folder), providing WunderID and WunderAPIKEY on the commandline, this wundergroundID will be set active.
Active means it is not necessary anymore in the future to provide the WunderID with its key on the commandline in database mode.
Activate a WundergroundID by providing the WundergroundID with its key.
Select specific WundergroundID (not making it the default for next time) by providing only ID without key.

## Convenient std call

The easiest call is to use *--dbauto*. It will look into db for the latest date being supplied with your active underground_id, and download all data from last entry till yesterday. No need for giving dates, id, key (all of this coming from database).

```bash
(web) joe@pi:~ $ ./wunderground_getdata.py --dbauto -p weatherdata
```

## Missing data

In case your database is inconsistent (missing dates in database), use *--dbverify* param to find the days without data. Use it in combination with *--start* *--end* to restrict to time spans. Use *--force* to download new data into these gaps. With *--id* you might search vor weather data regarding different stations.

## Database description

The database by default is a [sqlite3](https://sqlite.org/index.html) database.
Find full database scheme at [pony.orm](https://editor.ponyorm.com/user/turofecs/WeatherDB/designer).
![database structure](db_structure.png)
